import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, ErrorHandler, NgModule} from '@angular/core';
import {AlertController, IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';

import {MyApp} from './app.component';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {RestangularModule} from 'ngx-restangular';
import { AuthProvider } from '../providers/auth/auth';
import {IonicStorageModule} from '@ionic/storage';

export function onAppInit (auth: AuthProvider) {
    return (): Promise<void> => {
        return new Promise<void>(async (resolve) => {
            try {
                await auth.checkTokenOnStart();
            }
            catch (err) {
               console.log(err);
            }
            finally {
                resolve();
            }
        });
    };
}

export function RestangularConfigFactory(RestangularProvider, alertCtrl: AlertController, auth: AuthProvider) {

    // Setting up attachment of header token
    RestangularProvider.addFullRequestInterceptor((element, operation, path, url, headers, params) => {

        return {
            headers: Object.assign({}, headers, {Authorization: `Bearer ${auth.token}`})
        };
    });

    RestangularProvider.setBaseUrl('http://easymath.test/api/v1');


    RestangularProvider.addResponseInterceptor((data, operation, what, url, response) => {

        if (data.data == null){
            console.log(url, data);
            return data.data;
        }

        if (data.data) {
            console.log(url, data.data);
        }

        return data.data;

    });

    RestangularProvider.addErrorInterceptor((response, subject, responseHandler) => {

        console.log(response);

        if (response.error && response.error.message){
            alertCtrl.create({title:response.error.message, buttons: ['OK']}).present();
            return true;
        }

        if (response.data) {

            let title = response.data.message ? response.data.message : 'Server error';
            let message = '';

            if (response.data.errors){
                for (let errors in response.data.errors){
                    for (let error in response.data.errors[errors]){
                        message += ' ' + response.data.errors[errors][error];
                    }
                }
            }

            if (response.data.error && response.data.error.message){
                message += response.data.error.message;
            }

            alertCtrl.create({title:title, message: message, buttons: ['OK']}).present();
            return true;

        }



        return true;

    })

}

@NgModule({
    declarations: [
        MyApp,
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp),
        RestangularModule.forRoot([AlertController, AuthProvider], RestangularConfigFactory),
        IonicStorageModule.forRoot()
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        AuthProvider,
        {
            provide: APP_INITIALIZER,
            useFactory: onAppInit,
            multi: true,
            deps: [AuthProvider]
        },
    ]
})
export class AppModule {}
