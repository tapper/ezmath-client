import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, Validators} from '@angular/forms';
import {Restangular} from 'ngx-restangular';
import {AuthProvider} from '../../providers/auth/auth';
import {AuthStatuses} from '../../providers/auth/auth.status';

@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {

    form = this.fb.group({
        email: ['student1@test.com', [Validators.required, Validators.email]],
        password: ['123456', Validators.required],
    });

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                private fb: FormBuilder,
                private restangular: Restangular,
                private auth: AuthProvider) {}


    async submit () {

        if (this.form.invalid){
            return;
        }

        try {
            await this.requestToken();
            this.navCtrl.setRoot('HomePage');
        } catch (err) {
            console.log(err);
        }
    }

    async requestToken(): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {

                let payload: any = {};
                payload.email = this.form.value.email;
                payload.password = this.form.value.password;

                let response = await this.restangular.all('tokens').customPOST(payload).toPromise();

                await this.auth.setToken(response.api_token);
                await this.auth.setId(response.id);
                await this.auth.setStatus(AuthStatuses.AUTHENTICATED);

                resolve();
            } catch (err) {
                console.log(err);
                reject(err);
            }
        });
    }
}
