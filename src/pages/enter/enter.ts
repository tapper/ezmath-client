import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';

@IonicPage()
@Component({
    selector: 'page-enter',
    templateUrl: 'enter.html',
})
export class EnterPage {

    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }

    goToRegister() {
        this.navCtrl.push('RegisterPage');
    }

    goToLogin () {
        this.navCtrl.push('LoginPage');
    }

}
