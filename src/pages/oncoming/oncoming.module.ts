import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OncomingPage } from './oncoming';

@NgModule({
  declarations: [
    OncomingPage,
  ],
  imports: [
    IonicPageModule.forChild(OncomingPage),
  ],
})
export class OncomingPageModule {}
