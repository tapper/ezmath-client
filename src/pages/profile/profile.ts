import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, Validators} from '@angular/forms';
import {Restangular} from 'ngx-restangular';
import {AuthProvider} from '../../providers/auth/auth';

@IonicPage()
@Component({
    selector: 'page-profile',
    templateUrl: 'profile.html',
})
export class ProfilePage {

    form = this.fb.group({
        name: ['', Validators.required],
        phone: ['', [Validators.required, Validators.minLength(9)]],
        city: ['', Validators.required],
        grade: ['א', Validators.required],
        level: ['א', Validators.required],
        school_name: ['', Validators.required],
        parent_name: ['', Validators.required],
        parent_phone: ['', [Validators.required, Validators.minLength(9)]],
    });
    grades = ['א', 'ב', 'ג', 'ד', 'ה', 'ן', 'ז', 'ח', 'ט'];
    levels = ['א', 'ב', 'ג', 'ד', 'ה', 'ן', 'ז', 'ח', 'ט'];
    id: number;
    passwordForm = this.fb.group({
        old_password: ['', Validators.required],
        new_password: ['', Validators.required],
    });

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public fb: FormBuilder,
                public restangular: Restangular,
                public auth: AuthProvider,
                public alertCtrl: AlertController) {
        this.auth.id$.subscribe(id => this.id = id);
    }

    async ionViewWillEnter () {
        try {
            let response = await this.restangular.one('students', this.id).get().toPromise();
            this.form.setValue({
                name: response.name,
                phone: response.phone,
                city: response.city,
                grade: response.grade,
                level: response.level,
                school_name: response.school_name,
                parent_name: response.parent_name,
                parent_phone: response.parent_phone,
            })
        } catch (err) {
            console.log(err);
        }
    }

    async submit() {

        if (!this.form.valid) {
            return;
        }

        let payload: any = {};
        payload.name = this.form.value.name;
        payload.phone = this.form.value.phone;
        payload.city = this.form.value.city;
        payload.grade = this.form.value.grade;
        payload.level = this.form.value.level;
        payload.school_name = this.form.value.school_name;
        payload.parent_name = this.form.value.parent_name;
        payload.parent_phone = this.form.value.parent_phone;

        try {
            await this.restangular.one('students', this.id).customPATCH(payload).toPromise();
            this.alertCtrl.create({title: 'Successfully updated', buttons: ['OK']}).present();
        } catch (err) {
            console.log(err);
        }
    }

    async submitPassword () {

        if (!this.passwordForm.valid) {
            return;
        }

        let payload: any = {};
        payload.old_password = this.passwordForm.value.old_password;
        payload.new_password = this.passwordForm.value.new_password;

        try {
            await this.restangular.one('students', this.id).customPOST(payload, 'password').toPromise();
            this.passwordForm.reset();
            this.alertCtrl.create({title: 'Successfully updated', buttons: ['OK']}).present();
        } catch (err) {
            console.log('err', err);
        }
    }

}
